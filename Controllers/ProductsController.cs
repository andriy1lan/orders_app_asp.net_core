using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using OrdersApp.Data;
using OrdersApp.Models;

namespace OrdersApp.Controllers
{
    
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController: ControllerBase
    {
        
        private readonly OrdersContext context;

        public ProductsController (OrdersContext dbContext)
        {
            this.context = dbContext;
        }

        // GET api/products
        [HttpGet]
        public ActionResult<IEnumerable<Product>> GetProducts()
        {
            var products = context.Products.ToList();
            if (products.Count!= 0)
            {
                return Ok(products);
            }
            else { return NotFound(); }
        }

        // GET api/products/5
        [HttpGet("{id}")]
        public ActionResult<Product> GetProduct(int id)
        {
            var product = context.Products.Find(id);
            if (product!= null)
            {
                return Ok(product);
            }
            else { return NotFound(); }
        }

        [HttpPost]
        public ActionResult<Product> PostProduct([FromBody]Product product)
        {
            context.Products.Add(product);
            context.SaveChanges();
            return CreatedAtAction(nameof(GetProduct), new { id = product.ProductId }, product);
        }

        [HttpDelete("{id}")]
        public ActionResult DeleteProduct(int id)
        {
            var product = context.Products.Find(id);

           if (product == null)
           {
           return NotFound();
           }

        context.Products.Remove(product);
        context.SaveChanges();
        return NoContent();
        }


    }
}