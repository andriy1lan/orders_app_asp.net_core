using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using OrdersApp.Data;
using OrdersApp.Models;

namespace OrdersApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        
        private readonly OrdersContext context;

        public OrdersController (OrdersContext dbContext)
        {
            this.context = dbContext;
        }

        // GET api/orders
        [HttpGet]
        public ActionResult<IEnumerable<OrderDTO>> GetOrders()
        {
            IQueryable<OrderDTO> orders=from o in context.Orders select new OrderDTO() 
                { OrderNumber = o.OrderNumber, CustomerName = o.Customer.Name,
                CustomerAddress = o.Customer.Address, TotalCost=o.OrderedProducts.Sum(p=>p.Price*p.Quantity),
                OrderStatus = o.Status.ToString() };
                return Ok(orders.AsEnumerable());
        }

        // GET api/orders/id
        [HttpGet("{id}")]
        public ActionResult<Order> GetOrder(int id)
        {
            Order order=(from o in context.Orders.Include(o=>o.Customer).Include(o=>o.OrderedProducts) where o.OrderNumber==id select o).FirstOrDefault();
            if (order == null)
            {
                return NotFound();
            }
            Console.WriteLine("777"+order.OrderDate);
            return Ok(order);
        }

        [HttpPost]
        public ActionResult<Order> PostOrder([FromBody]Order order)
        {
            Console.Write("oorr"+order.Status.ToString());
            if (!order.Status.ToString().Equals("New")) return BadRequest("The status of order should be New");
            ICollection<OrderedProduct> products=new List<OrderedProduct>();//Next line you can use...Select(o=>o.ProductId).ToList();
            //products=context.Products.Where(p=>order.Products.Select(o=>o.ProductId).Contains(p.ProductId)).ToList();
            //if (products.Count==0) return BadRequest("No Products Selected");
            products=ProductMapper.GetOrderedProductList(order.Products);
            //order.OrderedProducts=products;
            order.TotalCost=order.Products.Sum(p=>p.Price*p.Quantity);
            //context.Orders.Add(order);
            Customer customer=context.Customers.Find(order.Customer.CustomerId);
            
            foreach (OrderedProduct p in products) {
            Product product=context.Products.Find(p.ProductId);
            Console.WriteLine("071"+context.Entry(product).State);
               if (product.Quantity<p.Quantity) return BadRequest("Not enough "+p.ProductName+" just "+product.Quantity+" are available");
               product.Quantity-=p.Quantity;
               p.Order=order;
               Console.WriteLine("073"+context.Entry(product).State);
            }
            context.Orders.Add(order);
            context.OrderedProducts.AddRange(products);
            context.SaveChanges();
            return CreatedAtAction(nameof(GetOrder), new { id = order.OrderNumber }, order);
        }
        
        //[Route("api/controller/{one}/{two}")]
        [HttpGet("{id}/{quantity}/{size}")]
        public ActionResult<Product> AddProductToOrder([FromRoute]int id, [FromRoute]int quantity, [FromRoute]string size)
        {
               Product product=context.Products.Find(id);
               if (product==null) return NotFound();
               if (product.Quantity<quantity) return BadRequest("Not enough "+product.ProductName+" just "+product.Quantity+" are available");
               if (!product.Size.ToString().Equals(size)) return BadRequest("Incorrect: The product size is "+ product.Size);
               return Ok(product);

        }

    }
}