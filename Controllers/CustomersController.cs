using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using OrdersApp.Data;
using OrdersApp.Models;
namespace OrdersApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController: ControllerBase
    {
        private readonly OrdersContext context;

        public CustomersController (OrdersContext dbContext)
        {
            this.context = dbContext;
        }

        // GET api/orders
        [HttpGet]
        public ActionResult<IEnumerable<CustomerDTO>> GetCustomers()
        {
            IQueryable<CustomerDTO> customers=from c in context.Customers select new CustomerDTO() 
                { CustomerId = c.CustomerId, Name = c.Name, Address=c.Address,
                TotalOrderedCost=c.Orders.Sum(o=>o.TotalCost), OrdersCount=c.Orders.Count
                };
                return Ok(customers.AsEnumerable());
        }

        [HttpGet("{id}")]
        public ActionResult<CustomerDTO> GetCustomer(int id)
        {
            CustomerDTO customer=(from c in context.Customers where c.CustomerId==id select new CustomerDTO() 
                { CustomerId = c.CustomerId, Name = c.Name, Address=c.Address,
                TotalOrderedCost=c.Orders.Sum(o=>o.TotalCost), OrdersCount=c.Orders.Count
                }).FirstOrDefault();

            if (customer == null)
            {
                return NotFound();
            }
            return Ok(customer);
        }

        [HttpPost]
        public ActionResult<Customer> PostCustomer([FromBody] Customer customer)
        {
            context.Customers.Add(customer);
            context.SaveChanges();
            return CreatedAtAction(nameof(GetCustomer), new { id = customer.CustomerId }, customer);
        }
   
    }
}