using System.Reflection.Emit;
using Microsoft.EntityFrameworkCore;
//using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System.Collections.Generic;
using OrdersApp.Models;

namespace OrdersApp.Data
{
    public class OrdersContext: DbContext
    {
        public DbSet<Order> Orders {get; set;}
        public DbSet<Product> Products {get; set;}
        public DbSet<OrderedProduct> OrderedProducts {get; set;}
        public DbSet<Customer> Customers {get; set;}
        //public DbSet<OrderOrderedProduct> OrderProducts { get; set;}
        
        
        public OrdersContext(DbContextOptions<OrdersContext> options)
         : base(options)
        {
 
        }

        /* 
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
              modelBuilder.Entity<OrderOrderedProduct>()
                   .HasKey(op => new { op.OrderNumber, op.OrderedProductId });  
              modelBuilder.Entity<OrderOrderedProduct>()
                   .HasOne(op => op.Order)
                   .WithMany(o => o.OrderProducts)
                   .HasForeignKey(op => op.OrderNumber);  
              modelBuilder.Entity<OrderOrderedProduct>()
                   .HasOne(op => op.OrderedProduct)
                   .WithMany(p => p.OrderProducts)
                   .HasForeignKey(op => op.OrderedProductId);
        }
        */
        /* 
        protected override void OnModelCreating(Modelbuilder modelBuilder)
        {
               modelBuilder.Entity<Order>()
               .HasMany(o => o.OrderedProducts)
               .WithOne(p => p.Order).
               .OnDelete(DeleteBehavior.SetNull); //Or DeleteBehavior.Delete
        } */
        
    }
}