using System.ComponentModel;
using OrdersApp.Models;
using System;
using System.Collections.Generic;

namespace OrdersApp.Data
{
    public class ProductMapper
    {
        public static OrderedProduct GetOrderedProduct(Product product) {
             OrderedProduct orderedProduct=new OrderedProduct () {
             ProductId=product.ProductId, ProductName=product.ProductName,
             Category=product.Category, Size=product.Size,
             Quantity=product.Quantity, Price=product.Price,
             CreatedDate=product.CreatedDate, Comment=product.Comment
             };
             return orderedProduct;
        }

        public static List<OrderedProduct> GetOrderedProductList(IList<Product> products) {
            List<OrderedProduct> list=new List<OrderedProduct>();
            foreach(Product p in products) {
                  list.Add(GetOrderedProduct(p));
            }
            return list;
        }

    }
}