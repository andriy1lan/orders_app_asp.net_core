export class CustomerDTO {
    customerId: number;
    name: string;
    address: string;
    totalOrderedCost: number;
    ordersCount: number;
}
