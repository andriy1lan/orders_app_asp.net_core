import {Customer} from './customer.model';
import {Product} from './product.model';
export class Order {
    orderNumber: number;
    orderDate: string;
    customer: Customer;
    status: string;
    totalCost: number;
    products: Product[];
    orderedProducts: Product[];
    comments: string;
}
