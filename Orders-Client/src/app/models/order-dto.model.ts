export class OrderDTO {
    orderNumber: number;
    customerName: string;
    customerAddress: string;
    totalCost: number;
    orderStatus: string;
}
