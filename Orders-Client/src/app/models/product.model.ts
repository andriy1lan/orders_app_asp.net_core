export class Product {
    productId: number;
    productName: string;
    category: string;
    size: string;
    //order: Order;
    quantity: number;
    price: number;
    createdDate: string;
    comment: string;
}
