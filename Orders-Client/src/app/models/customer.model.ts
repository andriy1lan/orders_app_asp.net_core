export class Customer {
    customerId: number;
    createdDate: string;
    name: string;
    address: string;
    constructor(customerId?:number, createdDate?:string,
        name?:string, address?:string ) {
        this.customerId=customerId;
        this.createdDate=createdDate;
        this.name=name;
        this.address=address;
    }
}
