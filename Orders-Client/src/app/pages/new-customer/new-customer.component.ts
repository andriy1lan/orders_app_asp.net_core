import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../services/customer.service';
import { Customer} from '../../models/customer.model';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-new-customer',
  templateUrl: './new-customer.component.html',
  styleUrls: ['./new-customer.component.css']
})
export class NewCustomerComponent implements OnInit {

  curDate: Date;
  newCustomer: Customer=new Customer();
  isCreated:boolean=false;
  message:string="";

  constructor(private customerService :CustomerService, 
    private router:Router, private datePipe: DatePipe) { }

  ngOnInit() {
    this.curDate=new Date();
  }

  addCustomer() {
    this.newCustomer.createdDate=this.datePipe.transform(new Date(),'dd/MM/yyyy').toString();
    console.log(this.newCustomer);
    this.customerService.createCustomer(this.newCustomer).subscribe(
      data => {
        console.log(data);  
        this.isCreated = true;
        this.message="New customer added";
        this.router.navigateByUrl('/customers');
      },
      error => {
        console.log(JSON.stringify(error));
        this.message="Error in new customer creation";
      }
    );
  }

  cancelAddNewCustomer() {
    this.router.navigateByUrl('/customers');
  }
}
