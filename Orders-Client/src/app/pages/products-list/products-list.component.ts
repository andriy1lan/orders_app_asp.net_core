import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Product} from '../../models/product.model';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmationDialogComponent } from '../../shared/confirmation-dialog/confirmation-dialog.component';

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.css']
})
export class ProductsListComponent implements OnInit {

  productsList: Product[];

  constructor(private productService :ProductService, private router:Router,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.productService.getProductsList().subscribe(data=>{this.productsList=data;
      console.log(this.productsList); //get all products
      this.productsList.sort((x,y) => x.productId < y.productId ? -1 : 1)});
  }

  openDialog(id:number) {
    const dialogRef = this.dialog.open(ConfirmationDialogComponent, {
      width: '350px',
      data: "Do you confirm the deletion of the product?"
    });
    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log(result+" "+id);
        this.deleteProduct(id);
      }
    });
  }

  redirectToAddNewProduct() { 
    this.router.navigateByUrl('/products/new');
  }

  deleteProduct(id:number) {
    this.productService.deleteProduct(id)
      .subscribe(
        data => {
          console.log(data);
          let i:number;
          i=this.productsList.findIndex(x => x.productId === id);
          if (i!=-1) {this.productsList.splice(i, 1);}
          this.reloadData();
        },
        error => {console.log(error); 
          alert("Error in product deletion");})
  }

}
