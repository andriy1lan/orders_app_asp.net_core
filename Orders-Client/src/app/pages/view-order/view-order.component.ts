import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../services/order.service';
import { ProductService } from '../../services/product.service';
import { CustomerService } from '../../services/customer.service';
import { Order} from '../../models/order.model';
import { Product} from '../../models/product.model';
import { CustomerDTO} from '../../models/customer-dto.model';
import { Customer} from '../../models/customer.model';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.css']
})
export class ViewOrderComponent implements OnInit {

  order: Order;
  products: Product[];
  id: number;
  customersList: CustomerDTO[];

  constructor(private orderService :OrderService, private productService :ProductService,
    private customerService :CustomerService, private route: ActivatedRoute, 
    private router:Router, private datePipe: DatePipe) { }

  ngOnInit() {
    this.route.params.subscribe(params=>this.id=+params['orderNumber']);
    console.log(this.id); 
    this.orderService.getOrder(this.id).subscribe(data=>{console.log(data); 
      this.order=data; this.products=this.order.orderedProducts;
      console.log(this.order); //get order by id
  });
}

  editOrder() {
    this.router.navigateByUrl('/orders/edit');
  }

}
