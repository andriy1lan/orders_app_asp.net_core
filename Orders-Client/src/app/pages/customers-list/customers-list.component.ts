import { Component, OnInit } from '@angular/core';
import { CustomerService } from '../../services/customer.service';
import { CustomerDTO} from '../../models/customer-dto.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html',
  styleUrls: ['./customers-list.component.css']
})
export class CustomersListComponent implements OnInit {

  customersList: CustomerDTO[];

  constructor(private customerService :CustomerService, private router:Router) { }

  ngOnInit() {
    this.reloadData();
  }

  reloadData() {
    this.customerService.getCustomersList().subscribe(data=>{this.customersList=data;
      console.log(this.customersList); //get all customers
      this.customersList.sort((x,y) => x.customerId < y.customerId ? -1 : 1)});
  }

  redirectToAddNewCustomer() {  
    this.router.navigateByUrl('/customers/new');
  }

}
