import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../services/order.service';
import { ProductService } from '../../services/product.service';
import { CustomerService } from '../../services/customer.service';
import { Order} from '../../models/order.model';
import { Product} from '../../models/product.model';
import { CustomerDTO} from '../../models/customer-dto.model';
import { Customer} from '../../models/customer.model';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.css']
})
export class NewOrderComponent implements OnInit {

  curDate: Date;
  newOrder: Order=new Order();
  productsList: Product[];
  isCreated:boolean=false;
  message:string="";
  statuses: string[]=["New", "Paid", "Shipped", "Delivered", "Closed"];
  customerId: number;
  customersList: CustomerDTO[];

  constructor(private orderService :OrderService, private productService :ProductService,
    private customerService :CustomerService, private router:Router, private datePipe: DatePipe) { }

  ngOnInit() {
    this.newOrder.customer=new Customer();
    this.reloadData();
  }

  reloadData() {
      this.curDate=new Date();
      this.newOrder.status="New";
      this.productsList=JSON.parse(localStorage.getItem('products'));
      this.productsList!=null?this.productsList.sort((x,y) => x.productId < y.productId ? -1 : 1):null;
      this.customerService.getCustomersList().subscribe(data=>{this.customersList=data;
        this.customerId=this.customersList[0].customerId;});
      }
 
  addOrder() {
    this.newOrder.orderDate=this.datePipe.transform(new Date(),'dd/MM/yyyy').toString();
    var customer: Customer=new Customer();
    customer.customerId=this.customerId;
    this.newOrder.customer=customer;
    this.newOrder.products=this.productsList;
    console.log(this.newOrder);
    this.orderService.createOrder(this.newOrder).subscribe(
      data => {
        console.log(data);  
        this.isCreated = true;
        this.message="New order created";
        this.router.navigateByUrl('/orders');
      },
      error => {
        console.log(JSON.stringify(error));
        this.message="Error in new order creation";
      }
    );
    //this.newOrder=null;
  }

  onCustomerOptionSelected(value:string) {
    let intValue:number=+value; //parse string to number or parseInt
    this.customerId=this.customersList.filter(c=>c.customerId==intValue)[0].customerId;
   }

   onStatusOptionSelected(value:string) {
    this.newOrder.status=value;
    console.log(this.newOrder.status);
    //if (value!==this.newOrder.orderStatus) alert("this product size is "+this.newOrder.orderStatus);
   }

  cancelNewOrder() {
    this.router.navigateByUrl('/orders');
  }

  redirectToAddProductsToOrder() { 
    this.router.navigateByUrl('/orders/add/product');
  }

}
