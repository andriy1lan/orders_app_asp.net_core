import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Product} from '../../models/product.model';
import { ActivatedRoute, Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.css']
})
export class ViewProductComponent implements OnInit {

  id:number;
  product: Product;
  constructor(private productService :ProductService, private route: ActivatedRoute,
    private router:Router, private datePipe: DatePipe) { }

  ngOnInit() {
    this.route.params.subscribe(params=>this.id=+params['productId']);
    console.log(this.product); 
    this.productService.getProduct(this.id).subscribe(data=>{this.product=data;
      console.log(this.product); //get product by id
  });
 }

  editProduct() {
    this.router.navigateByUrl('/products/edit');
  }

}
