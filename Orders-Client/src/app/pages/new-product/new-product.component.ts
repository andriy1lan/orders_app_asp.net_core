import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Product} from '../../models/product.model';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {

  curDate: Date;
  newProduct: Product=new Product();
  isCreated:boolean=false;
  message:string="";
  categories: string[]=["Foods", "Beverages", "Others"];
  sizes: string[]=["Small", "Medium", "Big"];

  constructor(private productService :ProductService, 
    private router:Router, private datePipe: DatePipe) { }

  ngOnInit() {
    this.curDate=new Date();
    this.newProduct.category="Foods"; 
    this.newProduct.size="Small";
  }

  addProduct() {
    this.newProduct.createdDate=this.datePipe.transform(new Date(),'dd/MM/yyyy').toString();
    console.log(this.newProduct);
    this.productService.createProduct(this.newProduct).subscribe(
      data => {
        console.log(data);  
        this.isCreated = true;
        this.message="New product added";
        this.router.navigateByUrl('/products');
      },
      error => {
        console.log(JSON.stringify(error));
        this.message="Error in new product creation";
      }
    );
    //this.router.navigateByUrl('/products');
  }

  onCategoryOptionSelected(value:string) {
    this.newProduct.category=value;
    console.log(this.newProduct.category);
   }

   onSizeOptionSelected(value:string) {
    this.newProduct.size=value;
    console.log(this.newProduct.size);
   }

  cancelNewProduct() {
    this.router.navigateByUrl('/products');
  }

}
