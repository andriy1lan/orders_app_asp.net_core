import { Component, OnInit } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { OrderService } from '../../services/order.service';
import { OrderDTO} from '../../models/order-dto.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.css']
})
export class OrdersListComponent implements OnInit {
  
  ordersList: OrderDTO[];

  constructor(private orderService :OrderService, private router:Router) { }

  ngOnInit() {
    this.reloadData();
  }
  
  reloadData() {
    this.orderService.getOrdersList().subscribe(data=>{this.ordersList=data;
      console.log(this.ordersList); //get all orders
      this.ordersList.sort((x,y) => x.orderNumber < y.orderNumber ? -1 : 1)});
  }

  redirectToAddNewOrder() { 
    this.router.navigateByUrl('/orders/new');
    localStorage.setItem('products',null);
  }

}
