import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { OrderService } from '../../services/order.service';
import { Product} from '../../models/product.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-order-product',
  templateUrl: './order-product.component.html',
  styleUrls: ['./order-product.component.css']
})
export class OrderProductComponent implements OnInit {

  productsList: Product[];
  productNames: string[];
  productDetails: Product=new Product();
  size: string;
  sizes: string[]=["Small","Medium","Big"];
  quantity: number;
  constructor(private productService :ProductService, private orderService :OrderService,
    private router:Router) { }

  ngOnInit() {
    this.productService.getProductsList().subscribe(data=>{this.productsList=data; 
      console.log(this.productsList); this.productDetails=this.productsList[0];
      this.productNames=data.map(p=>p.productName)});
      this.size=this.productDetails.size;
      console.log(this.productDetails);
  }

  addProductToOrder() {
    let newProduct:Product;
    this.orderService.addProductToOrder(this.productDetails.productId, this.quantity,
      this.size).
    subscribe(data=>{newProduct=data;
    newProduct.quantity=this.quantity;
    let productsString:string=localStorage.getItem('products');
    let productss: Product[]=JSON.parse(productsString);
    if (productss==null) productss=[];
    if (newProduct!=null) productss.push(newProduct);
    console.log("33"+productss);
    console.log("334"+newProduct);
    localStorage.setItem('products',JSON.stringify(productss));
    console.log("44"+JSON.parse(localStorage.getItem('products')));
    this.router.navigateByUrl('/orders/new');
    });
    
  }

  cancelAddProductToOrder() {
    this.router.navigateByUrl('/orders/new');
  }

  onProductOptionSelected(value:string) {
   let intValue:number=+value; //parse string to number or parseInt
   this.productDetails=this.productsList.filter(p=>p.productId==intValue)[0];
  }

  onSizeOptionSelected(value:string) {
    if (value!==this.productDetails.size) alert("this product size is "+this.productDetails.size);
    this.size=this.sizes.filter(s=>s==value)[0];
   }

}
