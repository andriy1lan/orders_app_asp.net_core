import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { DatePipe } from '@angular/common';
import { AppComponent } from './app.component';
import { OrdersListComponent } from './pages/orders-list/orders-list.component';
import { NewOrderComponent } from './pages/new-order/new-order.component';
import { OrderProductComponent } from './pages/order-product/order-product.component';
import { AbstractOrderComponent } from './pages/abstract-order/abstract-order.component';
import { ProductsListComponent } from './pages/products-list/products-list.component';
import { NewProductComponent } from './pages/new-product/new-product.component';
import { CustomersListComponent } from './pages/customers-list/customers-list.component';
import { NewCustomerComponent } from './pages/new-customer/new-customer.component';
import { ViewProductComponent } from './pages/view-product/view-product.component';
import { ViewOrderComponent } from './pages/view-order/view-order.component';
import { MatDialogModule} from '@angular/material/dialog';
import { MatButtonModule} from '@angular/material/button';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { ConfirmationDialogComponent } from './shared/confirmation-dialog/confirmation-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    OrdersListComponent,
    NewOrderComponent,
    OrderProductComponent,
    AbstractOrderComponent,
    ProductsListComponent,
    NewProductComponent,
    CustomersListComponent,
    NewCustomerComponent,
    ViewProductComponent,
    ViewOrderComponent,
    ConfirmationDialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatDialogModule,
    MatButtonModule
  ],
  entryComponents: [
    ConfirmationDialogComponent
  ],
  providers: [DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
