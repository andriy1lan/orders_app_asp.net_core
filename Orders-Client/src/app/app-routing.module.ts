import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrdersListComponent } from './pages/orders-list/orders-list.component';
import { NewOrderComponent } from './pages/new-order/new-order.component';
import { OrderProductComponent } from './pages/order-product/order-product.component';
import { AbstractOrderComponent } from './pages/abstract-order/abstract-order.component';
import { ProductsListComponent } from './pages/products-list/products-list.component';
import { NewProductComponent } from './pages/new-product/new-product.component';
import { CustomersListComponent } from './pages/customers-list/customers-list.component';
import { NewCustomerComponent } from './pages/new-customer/new-customer.component';
import { ViewProductComponent } from './pages/view-product/view-product.component';
import { ViewOrderComponent } from './pages/view-order/view-order.component';

const routes: Routes = [
  { path: '', redirectTo: 'orders', pathMatch: 'full' },
  { path: 'orders', //component: AbstractOrderComponent,
  children: [{ path: 'new', component: NewOrderComponent},
  {path:':orderNumber',component: ViewOrderComponent},
  { path: 'add/product', component: OrderProductComponent},
  {path: '', component: OrdersListComponent}]},
  { path: 'products', //{path:'products/:productId',component: ViewProductComponent}
  children: [{ path: 'new', component: NewProductComponent},
  {path:':productId',component: ViewProductComponent},
  {path: '', component: ProductsListComponent}]},
  { path: 'customers',
  children: [{ path: 'new', component: NewCustomerComponent},
  {path: '', component: CustomersListComponent}]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
