import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Customer} from '../models/customer.model';
import { CustomerDTO} from '../models/customer-dto.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  private baseUrl = 'https://localhost:5001/api/customers';
  constructor(private http: HttpClient) { }

  getCustomersList(): Observable<CustomerDTO[]> {
    return this.http.get<CustomerDTO []>(`${this.baseUrl}`);
  }

  getCustomer(id: number): Observable<CustomerDTO> {
    return this.http.get<CustomerDTO>(`${this.baseUrl}/${id}`);
  }

  createCustomer(customer: Customer): Observable<Customer> {
    return this.http.post<Customer>(`${this.baseUrl}`, customer);
  }



}
