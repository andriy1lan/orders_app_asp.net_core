import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Order} from '../models/order.model';
import { OrderDTO} from '../models/order-dto.model';
import { Product} from '../models/product.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private baseUrl = 'https://localhost:5001/api/orders';
  constructor(private http: HttpClient) { }

  getOrdersList(): Observable<OrderDTO[]> {
    return this.http.get<OrderDTO []>(`${this.baseUrl}`);
  }

  getOrder(id: number): Observable<Order> {
    return this.http.get<Order>(`${this.baseUrl}/${id}`);
  }

  createOrder(order: Order): Observable<Order> {
    return this.http.post<Order>(`${this.baseUrl}`, order);
  }

  addProductToOrder(id:number, quantity:number, size:string) {
    return this.http.get<Product>(`${this.baseUrl}/${id}/${quantity}/${size}`);
  }

}
