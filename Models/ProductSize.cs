namespace OrdersApp.Models
{
    public enum ProductSize
    {
        Small,
        Medium,
        Big
    }
}