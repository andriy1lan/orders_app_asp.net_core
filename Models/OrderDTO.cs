namespace OrdersApp.Models
{
    public class OrderDTO
    {
        public int OrderNumber {get; set;}
        public string CustomerName {get; set;}
        public string CustomerAddress {get; set;}
        public float TotalCost {get; set;}
        public string OrderStatus {get; set;}

    }
}