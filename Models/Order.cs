using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace OrdersApp.Models
{
    public class Order
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderNumber {get; set;}
        //[JsonConverter(typeof(DateFormatConverter))]
        public DateTime OrderDate {get;set;}
        [Required]
        public virtual Customer Customer {get;set;}
        [JsonConverter(typeof(StringEnumConverter))]
        public OrderStatus? Status {get;set;}
        public float TotalCost {get;set;}
        [NotMapped]
        public IList<Product> Products {get;set;}
        //[JsonIgnore]
        public virtual ICollection<OrderedProduct> OrderedProducts  {get; set;}
        public string Comments {get;set;}
        
    }
}