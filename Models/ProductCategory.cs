namespace OrdersApp.Models
{
    public enum ProductCategory
    {
        Foods,
        Beverages,
        Others
    }
}