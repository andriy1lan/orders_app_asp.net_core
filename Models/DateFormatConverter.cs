using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace OrdersApp.Models
{
    public class DateFormatConverter : IsoDateTimeConverter
    {
        public DateFormatConverter()
        {
            DateTimeFormat = "dd/MM/yyyy";
        }

        public DateFormatConverter(string format)
        {
            DateTimeFormat = format;
        }
    }
}