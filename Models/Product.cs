using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace OrdersApp.Models
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int ProductId {get;set;}
        public string ProductName {get;set;}
        [JsonConverter(typeof(StringEnumConverter))]
        public ProductCategory? Category {get;set;}
        [JsonConverter(typeof(StringEnumConverter))]
        public ProductSize? Size {get;set;}
        //public virtual Order Order {get;set;}
        public int Quantity {get;set;}
        public float Price {get;set;}
        //[JsonConverter(typeof(DateFormatConverter), "dd/MM/yyyy")]
        public DateTime CreatedDate {get;set;}
        public string Comment {get;set;}

    }
}