using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace OrdersApp.Models
{
    public class OrderedProduct
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int OrderedProductId {get;set;}
        public int ProductId {get;set;}
        public string ProductName {get;set;}
        [JsonConverter(typeof(StringEnumConverter))]
        public ProductCategory? Category {get;set;}
        [JsonConverter(typeof(StringEnumConverter))]
        public ProductSize? Size {get;set;}
        //[ForeignKey("Order")]
        [JsonIgnore]
        public virtual Order Order {get;set;}
        public int Quantity {get;set;}
        public float Price {get;set;}
        public DateTime CreatedDate {get;set;}
        //public virtual ICollection<OrderOrderedProduct> OrderProducts  {get; set;}
        public string Comment {get;set;}
    }
}