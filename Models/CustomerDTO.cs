namespace OrdersApp.Models
{
    public class CustomerDTO
    {
        public int CustomerId {get;set;}
        public string Name {get;set;}
        public string Address {get;set;}
        public float TotalOrderedCost {get;set;}
        public int OrdersCount {get;set;}
    }
}