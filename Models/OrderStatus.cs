namespace OrdersApp.Models
{
    public enum OrderStatus
    {
       New, 
       Paid,
       Shipped, 
       Delivered, 
       Closed
    }
}