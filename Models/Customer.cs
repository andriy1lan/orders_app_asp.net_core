using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace OrdersApp.Models
{
    public class Customer
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int CustomerId {get;set;}
        public DateTime CreatedDate {get;set;}
        public string Name {get;set;}
        public string Address {get;set;}
        [JsonIgnore]
        public virtual ICollection<Order> Orders {get;set;}
    }
}